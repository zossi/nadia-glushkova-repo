Test Plan Template: <br>
**Front-end Integration Test Plan**<br>
Prepared by:<br>
*Nadezhda Glushkova* <br>
TABLE OF CONTENTS <br>
1.0 INTRODUCTION<br> 
2.0 OBJECTIVES <br>
2.1 Objectives<br>
3.0 SCOPE<br>
4.0 Testing Strategy<br>
5.0 Hardware Requirements and Environment Requirements <br>
6.0 Test Schedule <br>
7.0 Control Procedures<br>
8.0 Features to Be Tested<br> 
9.0 Features Not to Be Tested <br>
10.0 Resources/Roles & Responsibilities <br>
11.0 Schedules <br>
12.0 Significantly Impacted Departments (SIDs)  <br>
13.0  Tools <br>
14.0 Approvals <br>

1.0 **INTRODUCTION**

The purpose of this document is to capture the functionality of creating topic in Telerik Forum <br>

2.0 **OBJECTIVES** <br>
The tests of this Test Plan should validate from both the requirements perspective and business perspective that: <br>
    - All regular users can create a topic in forum;<br>
    - All functions of topic pop-up menu work correctly;<br>
    - The functionality is easy to use by the end-users;<br>
    - All points of integration within the functionality work as defined in requirements;<br>
    - Recovery procedures are correct and can be performed by users.<br>

The objective of forum topic creation functionality testing is to validate the funcionality of creating a topic in forum as a whole and with other operating systems. At the conclusion of testing, the project team and the test team will have a high level of confidence that the funcionality will work according to user requirements and will meet business needs. <br>
 
3.0 **SCOPE**

In this Front-end Intigration Test Plan it will be tested a forum’s topic creation functionality in Telerik forum for regular users in a forum. 

The Front-end Intigration Test plan  will not include testing for administration users. 


  
4.0 **TESTING STRATEGY**

Preparing Test Cases:

-	QA will be preparing test cases based on the exploratory testing. This will cover all scenarios for requirements.

Preparing Test Matrix:

-	QA will be preparing test matrix which maps test cases to respective requirement. This will ensure the coverage for requirements.

 Reviewing test cases and matrix:

-	Peer review will be conducted for test cases and test matrix by QA Lead
-	Any comments or suggestions on test cases and test coverage will be provided by reviewer respective Author of Test Case and Test Matrix
-	Suggestions or improvements will be re-worked by author and will be send for approval
-	Re-worked improvements will be reviewed and approved by reviewer

Creating Test Data:
-	Test data will be created by respective QA on client's developments/test site based on scenarios and Test cases.

 Executing Test Cases:

-	Test cases will be executed by respective QA on client's development/test site based on designed scenarios, test cases and Test data.

-	Test result (Actual Result, Pass/Fail) will updated in test case document Defect Logging and Reporting:

-	QA will be logging the defect/bugs in Word document, found during execution of test cases. After this, QA will inform respective developer about the defect/bugs.
	
4.1 Unit Testing Definition: 
Unit tests help to fix bugs early in the development cycle and save costs.

Participants:  Developers write a section of code to test a specific function .
Methodology: Unit testing will be performed manually .

4.2 System and Integration Testing Definition: 
System testing of software is testing conducted on a complete, integrated system to evaluate the system's compliance with its specified requirements.
Integration Testing verifies the entire system’s functionality according to the design specification.

Participants: QA team. 

4.3 Performance and Stress Testing Definition:
-	Check the optimal time the page is loaded
-	Check the operation of the system under
Participants: QA team. 
Methodology: It will be loaded automatically. 
 4.4 User Acceptance Testing Definition: 
 The purpose behind user acceptance testing is to conform that system is developed according to the specified user requirements and is ready for operational use. Acceptance testing is carried out at two levels - Alpha Testing. 
  Participants: User acceptance testing will be done at the Client.
  Methodology: Describe how the User Acceptance testing will be conducted. Who will write the test scripts for the testing, what would be sequence of events of User Acceptance Testing, and how will the testing activity take place?
 
 4.5 Automated Regression Testing Definition: 
    Retesting for fixed bugs will be done by respective QA once it is resolved by respective developer and bug/defect status will be updated accordingly. In certain cases, regression testing will be done if required.
  4.6 Alpha Testing Participants: 
    The alpha test is conducted at the developer's site by client.  

5.0 ** HARDWARE REQUIREMENTS Computers Modems  and ENVIRONMENT REQUIREMENTS *
        Hardware
    All test cases will be executed on the Development Server in the QA database environment.
    • One (1) networked HP Laser Jet printer.
    (2) Asus
    Intel Core i5-4460S 
    8GB DDR3 RAM: 
    24X Super-Multi DVD/RW drive
    (2) Dell Inspiron 3000
    Intel® Pentium® G3240 dual-core 3.1 GHz 3MB cache processor
    4GB RAM DDR3 1600 SDRAM memory
    1TB 7200 RPM serial ATA/600 hard drive, controller type: serial ATA
    (2) iMAC
    1.4GHz
    dual-core Intel Core i5 processor (Turbo Boost up
    to 2.7GHz) with 3MB shared L3 cache
    8GB of 1600MHz LPDDR3 onboard memory
    500GB (5400-rpm) hard drive
    Network
    • LAN
    - Synoptic 810 10Base-T Ethernet Concentrator
    - Category 5 cables to meet 10Base-T specifications
    Software
    • Telerik forum software
    • Server
    - GreenTree Accounting version 3.0
    - MS Windows 7 Pro operating system 
    - MS Windows 8 Pro operating system
  
6.0 TEST SCHEDULE 
    Testing will take place 3 weeks prior to the launch date. The first round of testing should be
completed in 4 working days.
    Representive activities will take 5 days to be executed.  
    Reporting actitvities will take 5 days to be executed.  
   
7.0 **FEATURES TO BE TESTED** <br>
    Features to be tested include the following:
        ● As a user, logging into the website as a Telerik forum
        ● As a user, navigating the the pop-up menu for creating new topic
        ● As a user, attach file or picture
        ● As a user, adding hyperlink inside the description
        ● As a user, using for creating topic multiple units of the same time
        ● As a user, choosing the optional tag /notification for what you create topic for/
        ● As a user, choosing the cagegorization group
        ● As a user, trying to create new empty topic
        ● As a user, cancelling a pop-up menu for creating new topic
        
9.0 **FEATURES NOT TO BE TESTED**
  Mobile usability of forum through a mobile device will not be tested. Only desktop web browser
functionality will be tested.
 
10.0 **RESOURCES/ROLES & RESPONSIBILITIES**
  All members of QA Qualique group; The Test Manager is responsible for facilitating the testing project, coordinating availability and schedule of testers and training them as needed. Each tester should understand the expectations on completion date and level of quality. The Test Manager should also communicate any risks to the team. 

11.0 **SCHEDULES Major Deliverables Identify the deliverable documents.**
The following activities must be completed:
    - Test plan prepared.
    - Functional specifications written and delivered to the testing team
    - Environment should be ready for testing (test data, test logins, test information,
etc).
    - Perform the tests.
    - Prepare test summary report.

12.0 **SIGNIFICANTLY IMPACTED DEPARTMENTS (SIDs)**
   Department/Business Area Bus. Manager Tester(s)

<br>
13.0 **TOOLS **

Manual, Selenium
Microsoft Excel, 
Jira
<br>
14.0 **Exit criteria**
    1.	 A certain level of requirements coverage has been achieved.
    2.	No high priority or severe bugs are left outstanding.
    3.	All high-risk areas have been fully tested, with only minor residual risks left outstanding.
    4.	Cost – when the budget has been spent.
    5.	The schedule has been achieved


End.


