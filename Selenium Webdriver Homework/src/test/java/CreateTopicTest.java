import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CreateTopicTest {
   private WebDriver driver;


    public CreateTopicTest(){

    }
    @Before
    public void setupBrowser() {



        System.setProperty("webdriver.chrome.driver", "C:\\Driver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://stage-forum.telerikacademy.com/");
    }


    public void logIn(){

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[@id=\"site-logo\"]")));
        driver.findElement(By.xpath("//button[contains(.,\"Log In\")]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='next']")));
        driver.findElement(By.xpath("//input[@placeholder=\"Email\"]")).sendKeys("stanislav.nikolov.a28@learn.telerikacademy.com");
        driver.findElement(By.xpath("//input[@placeholder=\"Password\"]")).sendKeys("Mechka");
        driver.findElement(By.id("next")).click();

    }



    @Test
    public void successfulCreationOfForumTopic(){
        logIn();
        WebDriverWait wait = new WebDriverWait(driver, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create-topic")));
        driver.findElement(By.id("create-topic")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@aria-label=\"Create Topic\"]")));

        WebElement titleTopic = driver.findElement(By.id("reply-title"));
        titleTopic.sendKeys("This is test case");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,\"d-editor-textarea-wrapper\")]/textarea")));
        WebElement description = driver.findElement(By.xpath("//div[contains(@class,\"d-editor-textarea-wrapper\")]/textarea"));
        String randomStr = "Hello, I Want to make test" + Math.random();
        description.sendKeys(randomStr);

        driver.findElement(By.xpath("//button[@aria-label=\"Create Topic\"]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.fancy-title")));
        WebElement displayedTitleOfTopic = driver.findElement(By.cssSelector("a.fancy-title"));
        Assert.assertTrue("Title validation is displayed",displayedTitleOfTopic.isDisplayed());

        WebElement displayedReplyBtn = driver.findElement(By.xpath("//button[@aria-label=\"Reply\"]"));
        Assert.assertTrue("Replay Button is displayed", displayedReplyBtn.isDisplayed());
    }

    @After
    public void tearDown(){

        driver.quit();
    }

}
