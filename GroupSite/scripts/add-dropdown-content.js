(() => {
    const $dropdown = $('.dropdown');
    const $dropdownContent = $('<div>');
    $dropdownContent.addClass('dropdown-content');

    // const teamAsText = '{ "members" : [' +
    //     '{ "name":"Андрей" , "pageName":"team-member-1" },' +
    //     '{ "name":"Вангел" , "pageName":"team-member-2" },' +
    //     '{ "name":"Надежда" , "pageName":"team-member-3" },' +
    //     '{ "name":"Станислав" , "pageName":"team-member-4" } ]}';
    // const team = JSON.parse(teamAsText);

    $.getJSON('./data/navbar-team.json', function (team) {
        team.members.forEach((member) => {
            $dropdownContent.append(`<a href="${member.pageName}.html">${member.name}</a>`);
        });
    });

    $dropdown.append($dropdownContent);

})();
