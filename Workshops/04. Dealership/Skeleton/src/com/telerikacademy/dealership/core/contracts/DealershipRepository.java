package com.telerikacademy.dealership.core.contracts;

import com.telerikacademy.dealership.models.contracts.User;

import java.util.List;

public interface DealershipRepository {
    
    public List<User> getUsers();
    
    public User getLoggedUser();
    
    public void setLoggedUser(User newUser);
    
    public void addUser(User userToAdd);
    
}
