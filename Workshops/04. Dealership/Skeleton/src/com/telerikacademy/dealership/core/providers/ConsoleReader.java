package com.telerikacademy.dealership.core.providers;


import com.telerikacademy.dealership.core.contracts.Reader;

import java.util.Scanner;

public class ConsoleReader implements Reader {
    
    private final Scanner scanner;
    
    public ConsoleReader() {
        scanner = new Scanner(System.in);
    }
    
    public String readLine() {
        return scanner.nextLine();
    }
    
}
