package com.telerikacademy.dealership.models.contracts;

public interface Motorcycle extends Vehicle {
    
    String getCategory();
    
}
