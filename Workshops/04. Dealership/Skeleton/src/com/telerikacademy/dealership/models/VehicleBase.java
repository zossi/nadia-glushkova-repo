package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class VehicleBase implements Vehicle {

    private final static String TYPE_FIELD = "Vehical type";
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    private String make;
    private String model;
    private int wheels;

    private double price;
    VehicleType vehicleType;
    private List<Comment> comments;
    
    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(VehicleType vehicleType, String make, String model, double price) {

        setVehicleType(vehicleType);
        setMake(make);
        setModel(model);
        setWheels(wheels);
        setPrice(price);
        comments = new ArrayList<>();

    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price){
        Validator.ValidateDecimalRange(price,ModelsConstants.MIN_PRICE,ModelsConstants.MAX_PRICE, String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,PRICE_FIELD,ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE));
        this.price=price;
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    private void setWheels(int wheels) {
         this.wheels = wheels;
    }

    @Override
    public String getMake() {
        return make;
    }

    private void setMake(String make) {
        Validator.ValidateNull(make,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MAKE_FIELD) );
        Validator.ValidateIntRange(make.length(),ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH, String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,MAKE_FIELD, ModelsConstants.MIN_MAKE_LENGTH,ModelsConstants.MAX_MAKE_LENGTH));
        this.make = make;
    }
    @Override
    public String getModel() {
        return model;
    }

    private void setModel(String model) {
        Validator.ValidateNull(model,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,MODEL_FIELD) );
        Validator.ValidateIntRange(model.length(),ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MAKE_LENGTH, String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,MODEL_FIELD, ModelsConstants.MIN_MODEL_LENGTH,ModelsConstants.MAX_MODEL_LENGTH));
        this.model = model;
    }
    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }
    private void setVehicleType(VehicleType vehicleType){
       Validator.ValidateNull(vehicleType,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,TYPE_FIELD) );
        this.vehicleType = vehicleType;
    }
    @Override
    public void removeComment(Comment comment) {

       Validator.ValidateNull(comment, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,COMMENTS_HEADER));
        if (comments.contains(comment)){
            comments.remove(comment);
        }

    }

    @Override
    public void addComment(Comment comment) {
       Validator.ValidateNull(comment, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,COMMENTS_HEADER));
        comments.add(comment);
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());

        builder.append(String.format("%s: %s", MAKE_FIELD, this.getMake())).append(System.lineSeparator());
        builder.append(String.format("%s: %s",MODEL_FIELD, this.getModel())).append(System.lineSeparator());
        builder.append(String.format("%s: %s",WHEELS_FIELD, this.getWheels())).append(System.lineSeparator());
        builder.append(String.format("%s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }
    

    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }


}
