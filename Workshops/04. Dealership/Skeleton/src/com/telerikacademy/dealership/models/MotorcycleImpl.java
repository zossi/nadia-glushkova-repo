package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import com.telerikacademy.dealership.models.common.Validator;


public class MotorcycleImpl extends VehicleBase implements Motorcycle {
    private final String CATEGORY_FIELD = "Category";
    private String category;

    public MotorcycleImpl( String make, String model, double price, String category) {
        super(VehicleType.MOTORCYCLE, make, model,price);
        setCategory(category);
    }

    @Override
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        Validator.ValidateNull(category,String.format(ModelsConstants.FIELD_CANNOT_BE_NULL,CATEGORY_FIELD));
        Validator.ValidateIntRange(category.length(),ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH, String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,CATEGORY_FIELD, ModelsConstants.MIN_CATEGORY_LENGTH,ModelsConstants.MAX_CATEGORY_LENGTH));
        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %s",CATEGORY_FIELD, this.getCategory());
    }
}
