package com.telerikacademy.dealership.models.contracts;

public interface Truck extends Vehicle {
    
    int getWeightCapacity();
    
}
