package com.telerikacademy.dealership.models.contracts;

import com.telerikacademy.dealership.models.common.enums.Role;

import java.util.List;

public interface User {
    
    String getUsername();
    
    String getFirstName();
    
    String getLastName();
    
    String getPassword();
    
    Role getRole();
    
    List<Vehicle> getVehicles();
    
    void addVehicle(Vehicle vehicle);
    
    void removeVehicle(Vehicle vehicle);
    
    void addComment(Comment commentToAdd, Vehicle vehicleToAddComment);
    
    void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment);
    
    String printVehicles();
    
}
