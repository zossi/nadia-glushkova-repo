package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;
import com.telerikacademy.dealership.models.common.Validator;

public class TruckImpl extends VehicleBase implements Truck {
    private final static String WEIGHT_CAPACITY_FIELD = "Weight capacity";
    private int weight_Capacity;

    public TruckImpl(String make, String model, double price, int weight_Capacity) {
        super(VehicleType.TRUCK, make, model, price);
        Validator.ValidateIntRange(weight_Capacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY, String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, WEIGHT_CAPACITY_FIELD, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
        this.weight_Capacity = weight_Capacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %dt", WEIGHT_CAPACITY_FIELD, this.getWeightCapacity());
    }

    @Override
    public int getWeightCapacity() {
        return weight_Capacity;
    }
}
