package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

import java.lang.reflect.Type;

public class TrainImpl extends VehicleBase implements Train {
    public static final int MIN_PASSENGER_CAPACITY = 30;
    public static final int MAX_PASSENGER_CAPACITY = 150;
    public static final int MIN_VALUE_CARTS = 1;
    public static final int MAX_VALUE_CARTS = 15;
    private final String NOT_IN_QUANTITY = "A train cannot have less than 30 passengers or more than 150 passengers";
    private final String EXCEPTION_CARTS="A train cannot have less than 1 cart or more than 15 carts.";
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
         setCarts(carts);
    }

    public int getCarts() {
        return carts;
    }

    public void setCarts(int carts) {
        if (carts < MIN_VALUE_CARTS || carts > MAX_VALUE_CARTS) {
            throw new IllegalArgumentException(EXCEPTION_CARTS);
        }

        this.carts = carts;
    }

    @Override
    public String toString() {
        return "Train ----" +
                "Passenger capacity: " + super.getPassengerCapacity() +
                "Price per kilometer: " + super.getPricePerKilometer() +
                "Vehicle type: " + type +
                "Carts amount:" + carts;
    }

    @Override
    public void validatePassengerCapacity() {
        if (super.getPassengerCapacity() < MIN_PASSENGER_CAPACITY || super.getPassengerCapacity()> MAX_PASSENGER_CAPACITY) {
         throw new IllegalArgumentException(NOT_IN_QUANTITY);
        }

    }



    @Override
    public String print() {
        return null;
    }
}
