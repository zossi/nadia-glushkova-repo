package com.telerikacademy.agency.models.contracts;
import com.telerikacademy.agency.models.JourneyImpl;

public interface Ticket extends Printable {
    
    double getAdministrativeCosts();
    
    JourneyImpl getJourney();
    
    double calculatePrice();
    
}
