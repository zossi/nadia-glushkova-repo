package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    JourneyImpl journey;
    private double administrativeCosts;

    public TicketImpl(JourneyImpl journey, double administrativeCosts) {
        setJourney(journey);
        setAdministrativeCosts(administrativeCosts);
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public JourneyImpl getJourney() {
        return journey;
    }

    public void setJourney(JourneyImpl journey) {
        this.journey = journey;
    }

    public void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double calculatePrice() {
        return journey.calculateTravelCosts() * this.administrativeCosts;
    }

    @Override
    public String toString() {
        return "Ticket ---" +
                "Destination: " + journey.getDestination() +
                "Price: " + calculatePrice();

    }

    @Override
    public String print() {
        return toString();
    }
}
