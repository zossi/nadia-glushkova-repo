package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
    public static final int MIN_PASSENGER_CAPACITY = 30;
    public static final int MAX_PASSENGER_CAPACITY = 150;
    private final String NOT_IN_QUANTITY = "A train cannot have less than 30 passengers or more than 150 passengers";
    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);

        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public String toString() {
        return "AirplaneImpl ---- %n" +
                "Passenger capacity: " + super.getPassengerCapacity() +
                "Price per kilometer: " + super.getPricePerKilometer() +
                "Vehicle type: " + super.type +
                "Has free food:" + hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return false;
    }

    @Override
    public String print() {
        return toString();
    }

    public boolean validatePassengerCapacity() {
        if (super.getPassengerCapacity() < MIN_PASSENGER_CAPACITY || super.getPassengerCapacity()> MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(NOT_IN_QUANTITY);
        }
        return true;

    }
}
