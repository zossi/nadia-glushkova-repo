package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;

public class VehicleBase {

    public static final double MIN_VALUE_PRICE_PRE_KM = 0.10;
    public static final double MAX_VALUE_PRICE_PER_KM = 2.50;
    public static final String EXCEPTION_PRICE_PER_KM = "A vehicle with a price per kilometer lower than \\$0.10 or higher than \\$2.50 cannot exist!";
    private final String TYPE_CANT_BE_NULL = "Type can not be null";
    private final String PRICE_CANT_BE_ZERO_OR_NEGATIVE = "Price can not be zero or negative";
    public static final int MIN_PASSENGER_CAPACITY = 1;
    public static final int MAX_PASSENGER_CAPACITY = 800;
    private final String NOT_IN_QUANTITY = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private int passengerCapacity;
    private double pricePerKilometer;
    VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        validateType(type);
        this.type=type;

    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        validatePassengerCapacity();
        this.passengerCapacity = passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        validatePricePerKilometer();
        this.pricePerKilometer = pricePerKilometer;
    }

    private boolean validateType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
       return true;
    }
    public void validatePassengerCapacity(){
        if (getPassengerCapacity() < MIN_PASSENGER_CAPACITY || getPassengerCapacity()> MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(NOT_IN_QUANTITY);
        }

    };

    private boolean validatePricePerKilometer (){
        if (pricePerKilometer < MIN_VALUE_PRICE_PRE_KM || pricePerKilometer> MAX_VALUE_PRICE_PER_KM){
            throw new IllegalArgumentException(EXCEPTION_PRICE_PER_KM);
        }
        return true;

    }





}
