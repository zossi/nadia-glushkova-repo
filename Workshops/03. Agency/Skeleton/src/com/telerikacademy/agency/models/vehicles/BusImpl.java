package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;

public class BusImpl extends VehicleBase{
    public static final int MIN_PASSENGER_CAPACITY = 10;
    public static final int MAX_PASSENGER_CAPACITY = 50;
    private final String NOT_IN_QUANTITY =  "A bus cannot have less than 10 passengers or more than 50 passengers.";
    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);

    }

//    public String print() {
//        return String.format("Bus ----" + System.lineSeparator() +
//                "Start location: %s" + System.lineSeparator() +
//                "Destination: %s" + System.lineSeparator() +
//                "Distance: %d" + System.lineSeparator() +
//                "Vehicle type: %s" + System.lineSeparator() +
//                "Travel costs: %.2f" + System.lineSeparator(), super.getPassengerCapacity(), super.getPricePerKilometer()(), getDistance(), getVehicle().getType(), calculateTravelCosts());
//    }
    @Override
    public String toString() {
        return "Bus ----" +
                "Passenger capacity: " + super.getPassengerCapacity() +
                "Price per kilometer: " + super.getPricePerKilometer() +
                "Vehicle type: " + super.type;

    }

    @Override
    public boolean validatePassengerCapacity() {
        if (super.getPassengerCapacity() < MIN_PASSENGER_CAPACITY || super.getPassengerCapacity()> MAX_PASSENGER_CAPACITY) {
            throw new IllegalArgumentException(NOT_IN_QUANTITY);
        }
        return true;
    }
}
