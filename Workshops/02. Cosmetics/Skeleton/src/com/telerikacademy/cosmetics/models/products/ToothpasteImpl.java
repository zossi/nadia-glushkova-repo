package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.contracts.*;
import com.telerikacademy.cosmetics.models.common.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    public ToothpasteImpl(String name, String brand, double price, GenderType gender) {
        super(name, brand, price, gender);
    }
}
