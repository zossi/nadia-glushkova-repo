package com.telerikacademy.cosmetics.models.contracts;

import com.telerikacademy.cosmetics.models.common.*;
import com.telerikacademy.cosmetics.models.products.ProductBase;

public interface Cream extends Product {
    
    public ScentType getScent();
    
}
