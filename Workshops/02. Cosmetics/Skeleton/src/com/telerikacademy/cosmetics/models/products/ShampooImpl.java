package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import com.telerikacademy.cosmetics.models.contracts.*;

public class ShampooImpl extends ProductBase implements Shampoo {

    int milliliters;
    UsageType usageType;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usageType) {
        super(name, brand, price, gender);
        this.milliliters = milliliters;
        setUsage();
    }

    @Override
    public int getMilliliters() {
        validateMilliliters();
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return usageType;
    }

    private void setUsage (UsageType usageType){
        return this.usageType = usageType;
    }

    private boolean validateMilliliters (){
        if (milliliters <= 0){
            throw new NotImplementedException();
        }
        return true;
    }

    @Override
    public String print() {
        return null;
    }
}
