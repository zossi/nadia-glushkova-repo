package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.*;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {

    private static final int MAX_LENGTH_NAME = 10;
    private static final int MIN_LENGHT_NAME = 3;
    
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        validateName();
        products = new ArrayList<Product>();
    }

    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        throw new NotImplementedException();
    }
    
    public void removeProduct(Product product) {
        //validate
        throw new NotImplementedException();
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }
        //finish ProductBase class before implementing this method
        
        throw new NotImplementedException();
    }
    protected boolean validateName (){
        if (name == null || name.isEmpty()){
            throw new NotImplementedException();
        }
        if (this.name.length()< MIN_LENGHT_NAME || this.name.length()>MAX_LENGTH_NAME){
            throw new NotImplementedException();
        }

        return true;
    }
}
