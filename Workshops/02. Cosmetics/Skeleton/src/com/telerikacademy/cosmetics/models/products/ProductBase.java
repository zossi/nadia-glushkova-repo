package com.telerikacademy.cosmetics.models.products;

import  com.telerikacademy.cosmetics.models.common.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import com.telerikacademy.cosmetics.models.contracts.*;

public class ProductBase implements Product {

    private static final int MAX_LENGTH_NAME = 10;
    private static final int MIN_LENGHT_NAME = 3;
    private static final int MIN_LENGTH_BRAND_NAME = 2;
     String name;
     String brand;
     double price;
     GenderType gender;


    ProductBase(String name, String brand, double price, GenderType gender) {
        getName();
        getBrand();
        getPrice();
        getGender();
        this.gender = gender;
    }

    @Override
    public String getName() {
        validateName();
        return name;
    }

    @Override
    public String getBrand() {
        validateBrand();
        return brand;
    }

    @Override
    public double getPrice() {
        validatePrice();
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }


    protected boolean validateName (){
        if (name == null || name.isEmpty()){
            throw new NotImplementedException();
        }
        if (this.name.length()< MIN_LENGHT_NAME || this.name.length()>MAX_LENGTH_NAME){
            throw new NotImplementedException();
        }

    return true;
    }
    protected boolean validateBrand (){
        if (brand == null || brand.isEmpty()){
            throw new NotImplementedException();
        }
        if (brand.length()< MIN_LENGTH_BRAND_NAME || brand.length()>MAX_LENGTH_NAME){
            throw new NotImplementedException();
        }
        return true;
    }
    protected boolean validatePrice (){
        if (price <= 0){
            throw new NotImplementedException();
        }
        return true;
    }
}
