package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.contracts.*;
import com.telerikacademy.cosmetics.models.common.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class CreamImpl extends ProductBase implements Cream{
    ScentType scentType;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scentType) {
        super(name, brand, price, gender);
        this.scentType = scentType;
    }

    @Override
    public ScentType getScent() {
        return scentType;
    }

    @Override
    protected boolean validateName() {
        return super.validateName();
    }

    @Override
    protected boolean validateBrand() {
        return super.validateBrand();
    }

    @Override
    protected boolean validatePrice() {
        return super.validatePrice();
    }
}


