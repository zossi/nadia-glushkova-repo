|Test Case Title|Functionality|Prio|Techniques|
|---|---|---|---|
|Topic title should not be too long among the limit chars in field| Topic creation|Prio 1|BVA|
|Topic title should not be empty|Topic creation|Prio 1|BVA|
|Topic title can be empty|Topic creation|Prio 1|BVA|
|Description field should not be empty when create a new topic|Topic creation|Prio 1|BVA|
|Description field can be empty|Topic creation|Prio 1|BVA|
|Create a new topic with category Aplha QA 28, optional tag - important, with attachment, to an existing Forum| Topic creation|Prio 2|Pairwise testing; Use case testing|
|Create a new topic with title and description with Latin chars|Topic creation|Prio 2|Use case testing|
|Bold and Italic should work together|Topic creation|Prio 3|Pairwise testing,Use case testing|
|Quote whole post and Blockquote should work together|Topic creation|Prio 3|Pairwise testing, Use case testing|
|Attachment, hyperlink and inserting an emoji should work together|Topic creation|Prio 3|Pairwise testing,Use case testing|
|Bullet list and Number list should work together|Topic creation|Prio 3|Pairwise testing,Use case testing|
|Create a hurtful or inappropriate topic with default settings to an existing Forums|Topic creation|Prio 1|Use case testing|
|Description field should not be empty|Comment creation|Prio 1|BVA|
|Description field can be empty|Comment creation|Prio 1|BVA|
|Post field is overwrite with over the limit chars for the field|Comment creation|Prio 2|BVA|
|Bullet list and Number list should work together|Comment creation|Prio 3|Prairwise testing|
|Attachment , hyperlink and inserting an emoji should work together|Comment creation|Prio 3|Prairwise testing|
|Bold and Italic should work together|Comment creation|Prio 3|Prairwise testing|
|Quote whole post and Blockquote should work together|Comment creation|Prio 3|Prairwise testing|
|Post a short comment with  cyrillic letters|Comment creation|Prio 2|Use case testing|
|Post an empty comment|Comment creation|Prio 3|Equivalence partitioning|
|Post an anonymous comment|Comment creation|Prio 3|Use case testing|
|Bookmark and Flag on forum notification should work together|Notification| Prio 1 BVA|
|Share and Notification info on forum notification should work together| Notification |Prio 1|BVA|
|Notification disapear when user view it|Notification|Prio 3| State Transition testing|