Focus on topic creation, commenting and notifications
**Topic creation** test cases:

1.**Title**							
Topic title should not be too long among the limit chars in field	

**Description**: 
 - In order to create a new topic with default settings on a forum 
 - As a user of forum
 - I want to create a new topic

**Steps to reproduce**:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title" with many chars among the limit;
- You may provide a brief description that will display below the title in the Discussion Forums area;
- Click "Save" to save the topic, or "Save and New" to save the topic and create another one.


**Expected result**:  To create a new topic 

**Actual result:** The pop-up message to  inform you that you put too many char in "Title" field

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis

----------------------------------------------------------------------------------------------------------
2.  **Title**
Topic title should not be empty

**Description**: 
 - In order to create a new topic with default settings on a forum 
 - As a user of forum
 - I want to create a new topic

**Steps to reproduce**:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar
- Enter a short title in the text box "Title" ;
- You may provide a brief description that will display below the title in the Discussion Forums area;
- Click "Save" to save the topic, or "Save and New" to save the topic and create another one.


**Expected result**: To create a new topic

**Actual result:** To create a new topic

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis

-------------------------------
3.  **Title**
Topic title can be empty

**Description**: 
 - In order to create a new topic with default settings on a forum 
 - As a user of forum
 - I want to create a new topic

**Steps to reproduce**:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- You may provide a brief description that will display below the title in the Discussion Forums area;
- Click "Save" to save the topic, or "Save and New" to save the topic and create another one.


**Expected result**: To create a new topic without a title

**Actual result:** The pop-up message to  inform you that "Title is required".

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis
-----------------------------------------------------------------------------------------------------------
4.  **Title**							
Description field should not be empty when create a new topic	

**Description**: 
 - In order to create a new topic with default settings on a forum 
 - As a user of forum
 - I want to create a new topic

**Steps to reproduce**:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title" with many chars among the limit;
- You may provide a brief description that will display below the title in the Discussion Forums area;
- Click "Save" to save the topic.


**Expected result**: To create  a new topic; 

**Actual result:** Create a new topic

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis
---------------------------------------------------
5.  **Title**							
Description field can be empty 

**Description**: 
 - In order to create a new topic with default settings on a forum 
 - As a user of forum
 - I want to create a new topic

**Steps to reproduce**:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title" with many chars among the limit;
- 
- Click "Save" to save the topic.


**Expected result**: To create  a new topic; 

**Actual result:** The pop-up message to  inform you that "Post can't be empty"

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis

--------------------------------------------------
6.**Title: **
 - Create a new topic with category Aplha QA 28, optional tag - important, with attachment, to an existing Forum
    
Description: 
 - In order to create a new topic on a forum 
 - As a user of forum
 - I want to create a new topic, which should be restricted , unvisible for outside the selected group, with a short description and an attachment

Steps to reproduce:
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title";
- You may provide a brief description that will display below the title in the Discussion Forums area;
- From the toolbar select the button "attach file/pic" and attach any picture;
- Select any desired category group - Alpha QA 28
- Set an optional tag: important
- Click "Save" to save the topic.

**Expected result**: Create a new topic in forum to Alpha QA 28 with important flag and short descrioption and attachment ;

**Actual result:** Create a new topic in forum to Alpha QA 28 with important flag and short descrioption and attachment ;

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
-----------------------------------------------------------------
7.**Title: **
 -Attachment , hyperlink and inserting an emoji should work together
    
**Description: **
 - In order to create a new topic on a forum 
 - As a user of forum
 - I want to create a new topic with hyperlink and an attachment

**Steps to reproduce:**
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title";
-  From the toolbar select the button "attach file/pic" and attach any picture;
- From the toolbar select the hyperlink icon to insert hyperlink inside the description
- From the toolbar select the emoji icon to insert a chosen emoji inside the description
- Click "Save" to save the topic.

**Expected result**: Create a new topic in forum  with hyperlink and attachment and emoji;

**Actual result:** Create a new topic in forum with hyperlink and attachment and emoji;

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
-----------------------------------------------------------------------------------------------
8. **Title: **
 -Bold and Italic should work together
    
**Description: **
 - In order to create a new topic on a forum 
 - As a user of forum
 - I want to create a new topic with hyperlink and an attachment

**Steps to reproduce:**
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title";
- You may provide a brief description by using Bold and italic formatting that will display below the title in the Discussion Forums area;
- Click "Save" to save the topic.

**Expected result**: Create a new topic in forum 

**Actual result:** Create a new topic in forum 

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
---------------------------------------
9. **Title: **
 -Quote whole post and Blockquote should work together
    
**Description: **
 - In order to create a new topic on a forum 
 - As a user of forum
 - I want to create a new topic with hyperlink and an attachment

**Steps to reproduce:**
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title";
- You may provide a brief description by using Quote whole post and put some words in Blockquotes 
- Click "Save" to save the topic.

**Expected result**: Create a new topic in forum 

**Actual result:** Create a new topic in forum 

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
--------------------------------------------------------------
10. **Title: **
 -Bullet list and Number list should work together
    
**Description: **
 - In order to create a new topic on a forum 
 - As a user of forum
 - I want to create a new topic with Bullet and Number list

**Steps to reproduce:**
- Log in with your account;
- In the forum  select  and click "New Topic"  from navigation bar;
- Enter a title in the text box "Title";
- You may provide a brief description by using Bullet list and Number list
- Click "Save" to save the topic.

**Expected result**: Create a new topic in forum 

**Actual result:** Create a new topic in forum 

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
-----------------------------------------------------------------------------

**Comment** test cases:

1. **Title: **
Description field should not be empty 

**Description**: 
 - In order to create a new comment on a forum topic
 - As a user of forum
 - I want to create a new comment 

**Steps to reproduce**:
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply”.
- Write a short text .
- Click "Replay"

**Expected result**: To create  a new comment 

**Actual result:**  To create  a new comment 

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis
----------------------
2. **Title: **
Description field can be empty 

**Description**: 
 - In order to create a new comment on a forum topic
 - As a user of forum
 - I want to create a new comment 

**Steps to reproduce**:
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply”.
- Click "Replay"


**Expected result**: To create  a new comment .

**Actual result:**  The pop-up message to  inform you that "Post can't be empty".
Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis
--------------------------------------------------------------------------------
3 **Title: **
Post field is overwrite with over the limit chars for the field

**Description**: 
 - In order to create a new comment on a forum topic
 - As a user of forum
 - I want to create a new comment 

**Steps to reproduce**:
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply”.
- Provide a long post description in comment field over the limit chars
- Click "Replay".


**Expected result**: To create  a new comment 

**Actual result:**  The pop-up message to  inform you that you should remove some chars
Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Boundary Value Analysis
------------------------------------------------------------------------------------------
4.**Title: **
 -Bullet list and Number list should work together
    
**Description: **
 - In order to create a new comment on a forum 
 - As a user of forum
 - I want to create a new comment

**Steps to reproduce:**
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply”.
- Click on Bullet list icon and put some text;
- Click on Number list icon and make some list to do.
- Click "Replay".

**Expected result**: Create a new comment to a forum topic

**Actual result:** Create a new comment to a forum topic

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
----------------------------------
5.**Title: **
 -Attachment , hyperlink and inserting an emoji should work together
    
**Description: **
 - In order to create a new comment on a forum 
 - As a user of forum
 - I want to create a new comment

**Steps to reproduce:**
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply”.
- Click on landscape icon and attach any picture;
- From the toolbar select the hyperlink icon to insert hyperlink inside the description
- From the toolbar select the emoji icon to insert a chosen emoji inside the description
- Click "Replay"

**Expected result**: Create a new comment to a forum topic with hyperlink , attachment  and emoji ;

**Actual result:** Create a new comment to a forum topic with hyperlink and attachment and emoji ;

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 3		

**Technique**
Pairwise testing
Use case testing
-----------------------------------------------------------------------------------------------
6. **Title: **
 -Bold and Italic should work together
    
**Description: **
 - In order to create a new comment on a forum 
 - As a user of forum
 - I want to create a new comment

**Steps to reproduce:**
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply".
- You may provide a brief description by using Bold and Italic formatting;
-  Click "Replay"

**Expected result**: Create a new comment to a forum topic

**Actual result:** Create a new comment to a forum topic

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 3		

**Technique**
Pairwise testing
Use case testing
---------------------------------------
7. **Title: **
 -Quote whole post and Blockquote should work together
    
**Description: **
 - In order to create a new comment on a forum 
 - As a user of forum
 - I want to create a new comment

**Steps to reproduce:**
- Log in with your account;
- Go to the last published comment in the forum. 
- Click on button “Reply".
- You may provide a brief description by using Quote whole post and put some words in Blockquotes 
- Click "Save" to save the topic.

**Expected result**: Create a new comment to a forum topic 

**Actual result:** Create a new comment to a forum topic

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 2		

**Technique**
Pairwise testing
Use case testing
------------------------------------------------------------------------------------------

**Notification**  test cases:
1.**Title**
Bookmark and Flag on forum notification should work together.

**Description: **
 - In order to notify my about some changes in a forum topic
 - As a user of forum
 - I want to notify me 

**Steps to reproduce:**
- Log in with your account;
- Go to the last published topicin the forum. 
- Scroll to the last comment at the topic;
- Click on bookmark button and make your own properties on pop-up menu;
- Click "Save"
- Click on a Flag icon and chose a flag "It's Inappropriate" from pop-up menu;
- Click "Flag topic"

**Expected result**: Hide for me this topic, because it is inappropriete, but remind me because of the bookmark 

**Actual result:** Hide for me this topic, because it is inappropriete, but remind me because of the bookmark 

Additional info:
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Pairwise testing
Use case testing

----------------
2.**Title**
Share and Notification info on forum notification should work together.

**Description: **
 - In order to notify my about some changes in a forum topic
 - As a user of forum
 - I want to notify me 

**Steps to reproduce:**
- Log in with your account;
- Go to the last published topicin the forum. 
- Scroll to the last comment at the topic;
- Click on share button and share the link of the forum topic;
- Click "Facebook" icon button.
- Click on a Notification icon and chose Tracking ;


**Expected result**: Share the link of the visited forum topic and notify my by email for new comments in the forum topic 

**Actual result:** Share the link of the visited forum topic and notify my by email for new comments in the forum topic
Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 1		

**Technique**
Pairwise testing
Use case testing
<<<<<<< HEAD
------------------------------------------------
=======
----------------------------------
>>>>>>> 19a0c205ebabe0797b0cd7abc8351603c917f176
3. **Title**
Notification disapear when user views it
**Description: **
 - In order to notify my about some changes in a forum topic
 - As a user of forum
 - I want to notify me 

**Steps to reproduce:**
- Log in with your account;
- Go to the last published topicin the forum. 
- Scroll to the last comment at the topic;
- Click on a Notification icon and chose Tracking ;
- Make a new comment
- Check your profile if you recieve a new notification for the new comment
- Marks it like as "readed"

**Expected result**: Notification does not disappear when use reads/views it 

**Actual result:** Share the link of the visited forum topic and notify my by email for new comments in the forum topic

Tested with Chrome, Internet Explorer and Edge

**Priority**: 
Prio 3		

**Technique**
State Transition testing
<<<<<<< HEAD
=======

>>>>>>> 19a0c205ebabe0797b0cd7abc8351603c917f176
