package com.company;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public final class EventLog {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

    private final String description;
    private final LocalDateTime timeStamp;

    public EventLog(String description) {
        this.description = description;
        this.timeStamp =LocalDateTime.now();
    }

    public String getDescription() {
        if (this.description == null ) throw new IllegalArgumentException( "Description cannot be null");
        return description;
    }



    public LocalDateTime getTimeStamp() {

        return timeStamp;
    }


    public String viewInfo(){
              return String.format("[%s] %s", this.timeStamp.format(formatter), this.description);

    }


}