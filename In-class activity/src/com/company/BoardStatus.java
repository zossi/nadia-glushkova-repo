package com.company;

public enum BoardStatus {
    OPEN ("Open"), TODO ("ToDo"), IN_PROGRESS ("In Progress"), DONE ("Done"), VERIFIED ("Verified");


    private static  BoardStatus[] values = values();

    public BoardStatus prev() {
        return values[(ordinal() - 1  + values.length) % values.length];
    }

    public BoardStatus next() {
        return values[(ordinal() + 1) % values.length];
    }

    private String statusString;
    private BoardStatus(String status) {
        this.statusString = status;
    }

    @Override
    public String toString(){
        return statusString;
    }


}
