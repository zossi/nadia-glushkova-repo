package com.company;

import java.time.LocalDate;

public class Task extends BoardItem{
    public static final BoardStatus START_STATUS_ID = BoardStatus.TODO;
    private String assignee;


    public Task(String title, String assignee, LocalDate dueDate) {
        super(title, dueDate);
        setAssignee(assignee);
    }

    public String getAssignee() {
        return assignee;
    }

    private void setAssignee(String assignee) {
        if(validateAssignee()){
            super.logEvent(String.format("Assignee changed from %s to %s", getAssignee(), assignee));
            this.assignee = assignee;
        }
    }

    @Override
    protected void logEvent(String event) {
        super.logEvent(event);
    }

    @Override
    public void revertStatus() {
        if (validateStatus()) super.revertStatus();
    }

    @Override
    public void advanceStatus() {
        super.advanceStatus();
    }

   private Boolean validateAssignee (){
        if (getAssignee() == null || getAssignee().isEmpty()) {

            throw new IllegalArgumentException("The Assignee is empty!");
        } else if (getAssignee().length() < 5 || getAssignee().length()> 30){
            throw new IllegalArgumentException(" The Assinee must contain chars between 5 and 30!" );
        } else  return true;
    }

    private boolean validateStatus(){
        if (super.getStatus() == START_STATUS_ID) {
            logEvent(String.format("Can't revert, already at %s", super.getStatus()));
            return false;
        } else {
            return true;
        }

    }
}
