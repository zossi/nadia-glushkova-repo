package com.company;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class BoardItem {
    public static final BoardStatus START_STATUS_ID = BoardStatus.OPEN;
    public static final BoardStatus FINAL_STATUS_ID = BoardStatus.VERIFIED;
    private String title;
    private LocalDate dueDate;
    private BoardStatus boardStatus;
    private final List<EventLog> history = new ArrayList<>();


    public BoardItem(String title, LocalDate dueDate){

        setTitle(title);
        setDueDate(dueDate);
        this.boardStatus = BoardStatus.OPEN;
        logEvent (String.format("Item created: %s", viewInfo()));
    }
    public String getTitle(){ return title == null ? "":title; }
    public void setTitle(String title){

        if (title == null || title.length()==0){
            throw new IllegalArgumentException("Please provide a non-empty title");
        }
        if (title.length()<=5 ||title.length()>=30){
            throw new IllegalArgumentException("Please provide a title with length between 5 and 30 chars");
        }

        logEvent(String.format("Title changed from %s to %s", getTitle(), title));
        this.title = title;
    }

    public BoardStatus getStatus(){
        return boardStatus;
    }

    private void setStatus(BoardStatus status) {
        logEvent(String.format("Status changed from %s to %s", getStatus(), status));

        this.boardStatus = status;
    }
    public LocalDate getDueDate() { return dueDate; }

    protected void setDueDate(LocalDate dueDate){
          LocalDate currentDate = LocalDate.now();

            if (dueDate.isBefore(currentDate)){
                throw new IllegalArgumentException("Set a new correct date, please!");
            }
        logEvent(String.format("DueDate changed from %s to %s", getDueDate(), dueDate));
        this.dueDate =  dueDate;
    }

    public void revertStatus(){
        if (this.boardStatus == START_STATUS_ID){
            logEvent(String.format("Can't revert, already at %s", getStatus()));
        } else {
            setStatus(boardStatus.prev());
        }

    }

    public void advanceStatus(){

        if (this.boardStatus == FINAL_STATUS_ID){
            logEvent(String.format("Can't advance, already at %s", getStatus()));
        } else {
            setStatus(boardStatus.next());
        }

    }

    public String viewInfo(){
        
        String result = String.format ("Item created: '%s, [%S | %s]",this.title,this.boardStatus,this.dueDate);
        return result;
    }
    protected void logEvent(String event) {
        history.add(new EventLog(event));
    }
    public void displayHistory (){
        for (EventLog el: history){
            System.out.println(el.viewInfo());
        }
    }

//    @Override
//    public String toString() {
//        return "BoardItem{" +
//                "title='" + title + '\'' +
//                ", dueDate=" + dueDate +
//                ", boardStatus=" + boardStatus +
//                ", history=" + history +
//                '}';
//    }

}

