package com.company;

import java.time.LocalDate;


public class Main {

    public static void main(String[] args) {
//        BoardItem item1 = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
//        System.out.println(item1.title);
//        System.out.println(item1.dueDate);
//        System.out.println(item1.status);
//
//        BoardItem item = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
//        System.out.println(item.status);
//        item.status = item.advanceStatus();
//        System.out.println(item.status);
//        item.status = item.advanceStatus();
//        System.out.println(item.status);
//        item.status =item.revertStatus();
//        System.out.println(item.status);
//        System.out.println("------------------------");
//        System.out.println(item.viewInfo());

//        BoardItem item = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
//        item.advanceStatus();
//        BoardItem anotherItem = new BoardItem("Encrypt user data", LocalDate.now().plusDays(10));
//
//        Board.items.add(item);
//        Board.items.add(anotherItem);
//
////        Board.items.add(item);
////        Board.items.add(anotherItem);
//
//        for (BoardItem boardItem : Board.items) {
//            boardItem.advanceStatus();
//        }
//
//        for (BoardItem boardItem : Board.items) {
//            System.out.println(boardItem.viewInfo());
//
//        }
//        BoardItem item = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
//        item.advanceStatus(); // properly changing the status
//        item.advanceStatus();
//        System.out.println(item.viewInfo()); // Status: InProgress
//
//        item.status = Status.OPEN; // ??
//        System.out.println(item.viewInfo());; // Status: Open

        //BoardItem item_1 = new BoardItem("Rewrite everything", LocalDate.now().plusDays(2));

// compilation error if you uncomment the next line:
// item.title = "Rewrite everything immediately!!!";
// we made title private so it cannot be accessed directly anymore

//


//        EventLog log = new EventLog("An important thing happened");
//        System.out.println(log.getDescription());
//        System.out.println(log.viewInfo());
//
//        EventLog log2 = new EventLog(null);

//        BoardItem item = new BoardItem("Refactor this mess", LocalDate.now().plusDays(2));
//        item.setDueDate(item.getDueDate().plusYears(2));
//        item.setTitle("Not that important");
//        item.revertStatus();
//        item.advanceStatus();
//        item.revertStatus();
//
//        item.displayHistory();
//
//        System.out.println("\\n--------------\\n");
//
//        BoardItem anotherItem = new BoardItem("Don't refactor anything",  LocalDate.now().plusDays(10));
//        anotherItem.advanceStatus();
//        anotherItem.advanceStatus();
//        anotherItem.advanceStatus();
//        anotherItem.advanceStatus();
//        anotherItem.advanceStatus();
//        anotherItem.displayHistory();

//        BoardItem item1 = new BoardItem("Implement login/logout", LocalDate.now().plusDays(3));
//        BoardItem item2 = new BoardItem("Secure admin endpoints", LocalDate.now().plusDays(5));

//        Board.addItem(item1);
//        Board.addItem(item2);
//        //Board.addItem(item1);
//        //Board.addItem(item2);
//
//        System.out.println(Board.totalItems()); // count: 2
//

//        Task task = new Task("Test the application flow", "Pesho", LocalDate.now().plusDays(1));
//        System.out.println(task.getTitle());    // Test the application flow
//        System.out.println(task.getDueDate());  // 2020-09-16
//        System.out.println(task.getStatus());   // To Do
//        System.out.println(task.getAssignee()); // Pesho

        Issue issue = new Issue(
                "App flow tests?",
                "We need to test the App!",
                LocalDate.now().plusDays(1));
        issue.advanceStatus();
        issue.setDueDate(issue.getDueDate().plusDays(1));
        issue.displayHistory();

    }
}
