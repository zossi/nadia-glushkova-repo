package com.company;

import java.util.ArrayList;
import java.lang.IllegalArgumentException;

public class Board {

    private static ArrayList<BoardItem> items = new ArrayList<>();
    //private int count = 0;
    private Board() {

    }
    public static ArrayList<BoardItem> getItems() {
        return items;
    }

    private static void setItems(ArrayList<BoardItem> items) {
        Board.items = items;
    }

    public static void addItem(BoardItem item) {
        if (items.contains(item)) throw new IllegalArgumentException("Item already in the list");
        items.add(item);

    }

    public static int totalItems(){

        return items.size() ;
    }


}
