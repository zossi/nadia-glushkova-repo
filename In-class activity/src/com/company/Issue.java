package com.company;

import java.time.LocalDate;

public class Issue extends BoardItem {

   private final String description;

    public Issue(String title, String description, LocalDate dueDate) {
        super(title, dueDate);
        this.description = description;

    }
    public String getDescription() {
        validateAssignee();
        return description;
    }

    @Override
    protected void setDueDate(LocalDate dueDate) {
        super.setDueDate(dueDate);
    }

    @Override
    protected void logEvent(String event) {
        super.logEvent(event);
    }

    private Boolean validateAssignee (){
        if (getDescription() == null || getDescription() == "") {
            throw new IllegalArgumentException("No description!");
        }
        return true;
    }
}
